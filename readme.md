# team-bigger
A simple website to display the sizes of Blaseball teams.\
Hosted on neocities at https://team-bigger.neocities.org (updated on any change).\
Blaseball: https://blaseball.com\
Forked from team-big so I can continue to update it.
